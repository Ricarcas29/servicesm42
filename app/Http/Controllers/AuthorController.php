<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
class AuthorController extends Controller
{

    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    /** 
     * List the Authors.
     *
     * @return void
     */
    public function index()
    {
        $authors = Author::all();
        return $this->successResponse($authors);
    }
    /**
     * Create an author. 
     *
     * @return void
     */
    public function store(request $request)
    {
        $rules =[
            'name'=> 'required|max:100',
            'gender'=>'required|in:male,female',
            'country'=>'required|max:80'
        ];
        $this->validate($request,$rules);

        $author = Author::create($request->all());
        return $this->succesResponse($author,Response::HTTP_CREATED);

    }
    /**
     * Show an Author.
     *
     * @return void
     */
    public function show($author)
    {
        $author= Author::findOrFail($author);
        return $this->successResponse($authors);
    }
    /**
     * Update an author
     *
     * @return void
     */
    public function update($author)
    {
        //
    }
    /**
     * Destroy an author.
     *
     * @return void
     */
    public function destroy($author)
    {
        //
    }




}
